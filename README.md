# lua2php

[![npm package](https://img.shields.io/npm/v/lua2php.svg)](https://www.npmjs.org/package/lua2php)
[![npm downloads](http://img.shields.io/npm/dm/lua2php.svg)](https://www.npmjs.org/package/lua2php)

A Lua to PHP transpiler.

## Usage

### CLI

```bash
$ yarn add lua2php
$ npx lua2php a.lua
```

## Examples

Visit [examples](https://gitlab.com/the-language/lua2php/tree/master/example).

## Implementation

* Table => `(object)["array"=>[......]]`
* `#a` => `count(a->array)`

## Different from Lua5.1

* Can only get the length of a continuous array.
* No `loadstring`, metatable, user-defined iterator ...
* `table`, `unpack`, `tostring`, `print`, `error`, `string`, `pairs`, `ipairs` cannot be used as normal identifiers.
* `error` can only receive one string argument. Translated to `throw new Exception()`
* `print` can only receive one string argument.
* `string.sub` can only receive one string and two positive integers.
* The value of `... and ...`, `... or ...` is boolean.
* Multiple return or assignment or `for a,b,c in pairs(...)`(`c` is `nil`) or ... is not supported (except `...=unpack(...)`)
* No TCO
* The variables captured in closures in the loop are the variables in the last.
* `for k, v in pairs(...)`. There is no `+1` when `k` is a number.

# Projects using lua2php

* [The Language](https://gitlab.com/the-language/the-language) An experimental programming language.
