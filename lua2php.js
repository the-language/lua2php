/*
    The Language
    Copyright (C) 2018, 2019  Zaoqi <zaomir@outlook.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

*/

// [Polyfill] https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat
if (!Array.prototype.flat) {
  Array.prototype.flat = function(depth) {
    var flattend = [];
    (function flat(array, depth) {
      for (let el of array) {
        if (Array.isArray(el) && depth > 0) {
          flat(el, depth - 1);
        } else {
          flattend.push(el);
        }
      }
    })(this, Math.floor(depth) || 1);
    return flattend;
  };
}

const luaparse=require('luaparse')
const phpunparse=require('php-unparse')
const assert_strict = require('assert').strict
const phpunparse_options={
  indent: true,
  dontUseWhitespaces: false,
  shortArray: true,
  bracketsNewLine: true,
  forceNamespaceBrackets: false,
  collapseEmptyLines: true
}
function luatmpvar(x){// : LuaId -> LuaId
    return "lUa_"+x+"_LuA"
}
const luatmpvars_special={}
function luatmpvars_special_a(x){
    luatmpvars_special[x]=true
    return x
}
function luatmpvars_special_p(x){
    return x in luatmpvars_special
}
const luatmpvar_vararg=luatmpvars_special_a(luatmpvar('Vararg'))
const luatmpvar_i=luatmpvars_special_a(luatmpvar('I'))//循環中的整數i。
const luatmpvar_v=luatmpvars_special_a(luatmpvar('V'))//循環中的。
const luatmpvar_e=luatmpvars_special_a(luatmpvar('E'))//錯誤處理。

let tmpvar_count=0
function init_tmpvar_do(){
    tmpvar_count=0
}
function new_tmpvar_do(){ // : -> LuaId
    tmpvar_count++
    return "lUaTmP_"+tmpvar_count.toString()+"_LuAtMp"
}

function pre(x){
    const ast=luaparse.parse(x,{comments:false,scope:false,locations:true})
    return ast
}
exports.pre=pre
function post(x){
    return phpunparse(x,phpunparse_options)
}
exports.post=post

function print_loc_of(x){
    const {loc:{ start: { line: loc_start_line, column: loc_start_column }, end: { line: loc_end_line, column: loc_end_column } }}=x
    return `((start (line ${loc_start_line}) (column ${loc_start_column})) (end (line ${loc_end_line}) (column ${loc_end_column})))`
}

function new_set(){
    return {}
}
function list2set(xs){
    const ret={}
    for(const x of xs){
	assert_equal(typeof x,'string')
	assert_equal(ret[x],undefined)
	ret[x]=true
    }
    return ret
}
function object_keys(s){
    const ret=[]
    for(const k in s){
	ret.push(k)
    }
    ret.sort()
    return ret
}
const set2list=object_keys
function object_union_first_priority(x,y){
    return Object.assign(object_copy(y),x)
}
const set_union_first_priority=object_union_first_priority
function set_has_p(s,x){
    assert_equal(typeof x,'string')
    return x in s
}
function set_add(s,x,e=()=>null){
    assert_equal(typeof x,'string',e)
    assert_equal(s[x],undefined,e)
    s[x]=true
}
function set_may_add(s,x,e=()=>null){
    assert_equal(typeof x,'string',e)
    s[x]=true
}

function new_scope(su){
    // php_used_locals : Set<PHPId>
    // used_nonlocals : Set<LuaId> // Lua的。塊之外。
    // php_used_nonlocals : Set<PHPId> // PHP的。PHP的function之外。
    // php_nonlocals : Set<PHPId>
    // locals : HashMap<LuaId, PHPId>
    /// nonlocals : HashMap<LuaId, PHPId>
    function a(x){
	assert_notEqual(x,null)
	assert_notEqual(x,undefined)
	return x
    }
    return {type:'Scope',
	    php_nonlocals:a(su.php_nonlocals),
	    php_used_nonlocals:a(su.php_used_nonlocals),
	    used_nonlocals:new_set(),
	    locals:{},
	    php_used_locals:a(su.php_used_locals),
	    nonlocals:object_union_first_priority(su.locals,su.nonlocals)}
}
function new_phpfunction_scope(su){
    return {type:'Scope', php_nonlocals:set_union_first_priority(su.php_used_locals, su.php_nonlocals),php_used_nonlocals:new_set(),used_nonlocals:new_set(),locals:{},php_used_locals:new_set(),nonlocals:object_union_first_priority(su.locals,su.nonlocals)}
}
function new_global_scope(){
    return {type:'Scope', php_nonlocals:new_set(),php_used_nonlocals:new_set(),used_nonlocals:new_set(),locals:{},php_used_locals:new_set(),nonlocals:{}}
}
const no_scope={type:'NoScope'}
function scope_p(x){
    return x.type==='Scope'
}
function scope_nonlocals_has_p(s,x){
    return !scope_locals_has_p(s,x)
}
function scope_used_add(s,x){// x : LuaId
    assert_true(scope_p(s))
    assert_equal(typeof x, 'string')
    if(!(x in s.locals)){
	set_may_add(s.used_nonlocals,x)
	const pi=name_lua2php(x,s)
	if(!(pi in s.php_used_locals)){
	    set_may_add(s.php_used_nonlocals, pi)
	}
    }
}
function scope_local_add(s,x){// x : LuaId
    assert_true(scope_p(s))
    assert_equal(s.locals[x], undefined,()=>`Redefine "${x}"`)
    let c=0
    function i(){
	return c===0?x:x+'_'+c.toString()
    }
    while(i() in s.php_used_locals || i() in s.php_nonlocals){
	c++
    }
    const id=i()
    s.php_used_locals[id]=true
    s.locals[x]=id
    return
}
function scope_locals_get(x){
    return object_keys(x.locals)
}
function scope_locals_has_p(s,x){
    assert_true(scope_p(s))
    assert_equal(typeof x, 'string')
    return x in s.locals
}
function scope_php_used_nonlocals_get(x){
    return set2list(x.php_used_nonlocals)
}
function scope_name_not_global_p(x,scope){
    return (x in scope.locals)||(x in scope.nonlocals)
}
function name_lua2phpast(x,scope){
    if(luatmpvars_special_p(x)){
	return { kind: 'variable', name: name_lua2php(x,no_scope), byref: false, curly: false }
    }else if(scope_name_not_global_p(x,scope)){
	return { kind: 'variable', name: name_lua2php(x,scope), byref: false, curly: false }
    }else{
	assert_not_reserved_word(x)
	return {
	    kind: 'offsetlookup',
	    what:{ kind: 'variable', name: 'GLOBALS', byref: false, curly: false },
	    offset: { kind: 'string', value: x, isDoubleQuote: true } }
    }
}
function name_lua2phpast_scope_used_add(x,scope){
    if(luatmpvars_special_p(x)){
	return { kind: 'variable', name: name_lua2php(x,no_scope), byref: false, curly: false }
    }else if(scope_name_not_global_p(x,scope)){
	scope_used_add(scope,x)
	return { kind: 'variable', name: name_lua2php(x,scope), byref: false, curly: false }
    }else{
	assert_not_reserved_word(x)
	return {
	    kind: 'offsetlookup',
	    what:{ kind: 'variable', name: 'GLOBALS', byref: false, curly: false },
	    offset: { kind: 'string', value: x, isDoubleQuote: true } }
    }
}
function assert_not_reserved_word(x){
    for(const reserved_word of ["table","unpack","string","tostring","error","pairs","ipairs","print"]){
	assert_notEqual(x,reserved_word,()=>`Reserved word "${x}" is not available as normal identifiers`)
    }
}
function name_lua2php(x,scope){
    if(luatmpvars_special_p(x)){
	return x
    }
    assert_not_reserved_word(x)
    if(scope.type==='Scope'){
	if(x in scope.locals){return scope.locals[x]}
	if(x in scope.nonlocals){return scope.nonlocals[x]}
    }else{
	assert_equal(scope.type,'NoScope')
    }
    return x
}


function assert_equal(x,y,msg=()=>null){ // msg懶惰求值。
    assert_strict.equal(x,y,{ toString() { return msg(); } })
}
function assert_notEqual(x,y,msg=()=>null){
    assert_strict.notEqual(x,y,{ toString() { return msg(); } })
}
function assert_fail(msg=null){
    assert_strict.ok(false,msg)
}
function assert_true(x,msg=()=>null){
    assert_strict.ok(x,{ toString() { return msg(); } })
}

// 此Monad：帶多初始化代碼
// [Array<PHPStatement>, x]
function make_monad(ss,x){
    return [ss,x]
}
function unmake_monad(m){
    return m
}
function monad_return(x){
    return [[],x]
}
function monad_bind(m,f){
    const [ss1,x]=m
    const [ss2,r]=f(x)
    return [ss1.concat(ss2),r]
}
function monad_mapM(f,ms){// : Monad m => (a -> m b) -> [a] -> m [b]
    if(ms.length===0){
	return monad_return([])
    }else{
	return monad_bind(
	    f(ms[0]),
	    (x)=>
		monad_bind(
		    monad_mapM(f,ms.slice(1)),
		    (xs)=>monad_return([x].concat(xs))))
    }
}

const null_statement=false
function null_statement_p(x){
    return x===false
}
function not_null_statement_p(x){
    return !null_statement_p(x)
}

function object_copy(o){
    return Object.assign({},o)
}

function luaast_number_p3(x,scope){// -> True | False | 'unknown'
    if(x.type==='NumericLiteral'){
	return true
    }else if(x.type==='BinaryExpression'){
	if(x.operator in {"+":true,"-":true,"*":true,"/":true}){return true}
	if(x.operator in {"==":false,"~=":false,"<":false,">":false,"<=":false,">=":false,"..":false}){return false}
    }else if(x.type==='UnaryExpression'){
	if(x.operator in {"-":true,"#":true}){return true}
	if(x.operator in {"not":false}){return false}
    }else if(x.type in {StringLiteral:false,LogicalExpression:false,TableConstructorExpression:false,FunctionDeclaration:false}){
	return false
    }
    return 'unknown'
}
function luaast_number_sub(x,y,scope){
    //WIP
    return  {type: 'BinaryExpression',
	     operator: '-',
	     left:x,
	     right:y,
	    }
}
function luaast_number_sub1(x,scope){
    if(x.type==='NumericLiteral'){
	const ret=object_copy(x)
	ret.value-=1
	ret.raw=ret.value.toString()
	return ret
    }else if(x.type==='BinaryExpression'){
	if(x.operator==='+'){
	    if(x.left.type==='NumericLiteral'){
		const ret_left=object_copy(x.left)
		ret_left.value-=1
		ret_left.raw=ret_left.value.toString()
		const ret=object_copy(x)
		ret.left=ret_left
		return ret
	    }else if(x.right.type==='NumericLiteral'){
		const ret_right=object_copy(x.right)
		ret_right.value-=1
		ret_right.raw=ret_right.value.toString()
		const ret=object_copy(x)
		ret.right=ret_right
		return ret
	    }
	}else if(x.operator==='-'){
	    if(x.left.type==='NumericLiteral'){
		const ret_left=object_copy(x.left)
		ret_left.value-=1
		ret_left.raw=ret_left.value.toString()
		const ret=object_copy(x)
		ret.left=ret_left
		return ret
	    }else if(x.right.type==='NumericLiteral'){
		const ret_right=object_copy(x.right)
		ret_right.value+=1
		ret_right.raw=ret_right.value.toString()
		const ret=object_copy(x)
		ret.right=ret_right
		return ret
	    }
	}
    }
    return {type:'BinaryExpression',operator:'-',left:x,right:{ type: 'NumericLiteral',value: 1,raw: '1'}}
}
function luaast_string_array_p3(x,scope){// -> 'string' | 'array' | 'unknown'
    const ret_s='string'
    const ret_a='array'
    const ret_u='unknown'
    if(x.type==='StringLiteral'){
	return ret_s
    }else if(x.type==='TableConstructorExpression'){
	return ret_a
    }
    return ret_u
}

const phpast_NULL={kind: 'constref', name:{ kind: 'identifier', resolution: 'uqn', name: 'NULL' }}

function phpast_make_luaarray(x){ // x : phparray
    return {kind: 'parenthesis', inner:{kind:'cast',type: 'object',what:{
	kind: 'array',
	items: [ {
	    kind: 'entry',
	    key: { kind: 'string', value: 'array', isDoubleQuote: true },
	    value: x } ],
	shortForm: true }}}
}
function phpast_luaarray_lookup(what,offset){
    return {
	kind: 'offsetlookup',
	what: phpast_luaarray2phparray(what),
	offset: offset }
}
function phpast_luaarray_length(what){
   return {
       kind: 'call',
       what: { kind: 'identifier', resolution: 'uqn', name: 'count' },
       arguments: [phpast_luaarray2phparray(what)]}
}
function phpast_luaarray2phparray(what){
    return {
	kind: 'propertylookup',
	what: what,
	offset: { kind: 'constref', name: 'array' } }
}
function phpast_number_sub1(x){// WIP
    return {
	kind: 'bin',
	type: '-',
	left: x,
	right: { kind: 'number', value: '1' } }
}
function phpast_number_sub(x,y){//WIP
    return {
	kind: 'bin',
	type: '-',
	left: x,
	right: y }
}
function phpast_runtime_is_int(x){
    return {
	kind: 'call',
	what: { kind: 'identifier', resolution: 'uqn', name: 'is_int' },
	arguments: [ x ] }
}
function phpast_runtime_is_string(x){
    return {
	kind: 'call',
	what: { kind: 'identifier', resolution: 'uqn', name: 'is_string' },
	arguments: [ x ] }
}
function phpast_runtime_ifThenElse(x,t,f){
    return {kind: 'parenthesis', inner: {kind: 'retif', test: x, trueExpr: t, falseExpr: f}}
}

function phpast_make_phpblock(body){
    return { kind: 'block', children: body }
}

function evalChunk_not_monad_init(x){
    init_tmpvar_do()
    assert_equal(x.type,'Chunk')
    const bodys=x.body
    const scope=new_global_scope()
    const result_bodys=evalStatement_s(bodys,scope)
    const result={kind:"program",children:result_bodys,errors:[]}
    return result
}
exports.evalChunk=evalChunk_not_monad_init
function evalStatement(body,scope){
    const t=body.type
    if(t==='LocalStatement'){
	return evalLocalStatement(body,scope)
    }else if(t==='AssignmentStatement'){
	return evalAssignmentStatement(body,scope)
    }else if(t==='ReturnStatement'){
	return evalReturnStatement(body,scope)
    }else if(t==='CallStatement'){
	return evalExpression(body.expression,scope)
    }else if(t==='IfStatement'){
	return evalIfStatement(body,scope)
    }else if(t==='DoStatement'){
	return evalDoStatement(body,scope)
    }else if(t==='WhileStatement'){
	return evalWhileStatement(body,scope)
    }else if(t==='FunctionDeclaration'){
	return evalFunctionDeclaration_statement(body,scope)
    }else if(t==='ForNumericStatement'){
	return evalForNumericStatement(body,scope)
    }else if(t==='ForGenericStatement'){
	return evalForGenericStatement(body,scope)
    }else if(t=='BreakStatement'){
	return monad_return({ kind: 'break', level: null })
    }
    assert_fail(`Unsupported node "${t}" at ${print_loc_of(body)}`)
}
function evalForGenericStatement(x,scope){
    assert_equal(x.type, 'ForGenericStatement')
    const i_var=name_lua2phpast(luatmpvar_i,no_scope)
    const v_var=name_lua2phpast(luatmpvar_v,no_scope)
    const {variables, iterators,body}=x
    assert_equal(iterators.length,1)
    const [iterator]=iterators
    if(iterator.type==='TableCallExpression'){
	iterator.type='CallExpression'
	iterator.arguments=[iterator.arguments]
    }
    assert_equal(iterator.type,'CallExpression')
    const {base, arguments:args}=iterator
    assert_equal(base.type, 'Identifier')
    assert_equal(args.length,1)
    const [arg]=args
    if(base.name==='pairs'){
	assert_true(variables.length==2||variables.length==1)
	const [vark,varv=null]=variables
	assert_equal(vark.type,'Identifier')
	if(varv!==null){assert_equal(varv.type,'Identifier')}
	body.unshift({ type: 'LocalStatement', variables: [vark], init:[{ type: 'Identifier', name: luatmpvar_i}]})
	if(varv!==null){body.unshift({ type: 'LocalStatement', variables: [varv], init:[{ type: 'Identifier', name: luatmpvar_v}]})}
	return monad_bind(
	    evalExpression_to_const(arg,scope),
	    (result_arg)=>
		monad_return({
		    kind: 'foreach',
		    source:result_arg,
		    key:i_var,
		    value:v_var,
		    shortForm: false,
		    body: eval_emu_block_not_monad(body,scope) }))
    }else{
	assert_equal(base.name,'ipairs')
	assert_true(variables.length==2||variables.length==1)
	const [vark,varv=null]=variables
	assert_equal(vark.type,'Identifier')
	if(varv!==null){assert_equal(varv.type,'Identifier')}
	const tmpvar_name=new_tmpvar_do()
	const tmpvar={ type: 'Identifier', name:tmpvar_name}
	const stats=[]
	stats.push({type:'LocalStatement',variables:[tmpvar],init:[arg]})
	const body_2=varv===null?body:[{type:'LocalStatement',variables:[varv],init:[{type:'IndexExpression',base:tmpvar,index:vark}]}].concat(body)
	stats.push({ type: 'ForNumericStatement',
		     variable: vark,
		     start: { type: 'NumericLiteral', value: 1, raw: '1'},
		     end: { type: 'UnaryExpression',
			    operator: '#',
			    argument: tmpvar},
		     step: null,
		     body: body_2})
	return make_monad(evalStatement_s(stats,scope),null_statement)
    }
}
function evalForNumericStatement(x,scope){
    assert_equal(x.type, 'ForNumericStatement')
    const {variable, start, end, step, body} = x
    assert_equal(variable.type, 'Identifier')
    const ret_step=step===null?{ type: 'NumericLiteral',value: 1,raw: '1'}:step
    const i_var=name_lua2phpast(luatmpvar_i,no_scope)
    return monad_bind(
	evalExpression(start,scope),
	(result_start)=>
	    monad_bind(
		evalExpression_to_const(end,scope),
		(result_end)=>
		    monad_bind(
			evalExpression_to_const(ret_step,scope),
			(result_step)=>
			    monad_return({
				kind: 'for',
				init:
				[ {
				    kind: 'assign',
				    operator: '=',
				    left: i_var,
				    right: result_start } ],
				test:
				[{
				    kind: 'retif',
				    test:
				    {
					kind: 'bin',
					type: '>=',
					left:result_step,
					right:{ kind: 'number', value: '0' } },
				    trueExpr:
				    {
					kind: 'bin',
					type: '<=',
					left:i_var,
					right:result_end},
				    falseExpr:
				    {
					kind: 'bin',
					type: '>=',
					left:i_var,
					right:result_end} }],
				increment:
				[ {
				    kind: 'assign',
				    operator: '+=',
				    left: i_var,
				    right: result_step } ],
				shortForm: false,
				body: (()=>{
				    const l1={ type: 'LocalStatement', variables: [variable], init:[{ type: 'Identifier', name: luatmpvar_i}]}
				    body.unshift(l1)
				    const result_block=eval_emu_block_not_monad(body,scope)
				    return result_block
				})() }))))
}
function evalFunctionDeclaration_statement(x,scope){
    assert_equal(x.type, 'FunctionDeclaration')
    const x2=object_copy(x)
    x2.identifier=null
    x2.isLocal=false
    return evalStatement({ type: x.isLocal?'LocalStatement':'AssignmentStatement',variables: [ x.identifier ],init: [x2]},scope)
}
function evalWhileStatement(x,scope){
    assert_equal(x.type, 'WhileStatement')
    const {condition, body} = x
    return monad_return({
	kind: 'while',
	test: evalExpression_not_monad_or_pack(condition,scope),
	body: eval_emu_block_not_monad(body,scope),
	shortForm: false })
}
function evalStatement_s(xs,scope){
    return xs.map((line)=>{
	const [ss,x]=unmake_monad(evalStatement(line,scope))
	return ss.concat([x])
    }).flat().filter(not_null_statement_p)
}
function eval_emu_block_not_monad(xs,scope){ // 在循環並捕獲變量的情況下與Lua5.1不同。
    const inner_scope=new_scope(scope)
    const result={
	kind: 'block',
	children: evalStatement_s(xs,inner_scope)}
    return result
}
function evalDoStatement(body,scope){
    assert_equal(body.type,'DoStatement')
    return monad_return(eval_emu_block_not_monad(body.body,scope))
}
function evalIfStatement(x,scope){
    assert_equal(x.type,'IfStatement')
    return evalIfStatement_clauses(x.clauses,scope)
}
function evalIfStatement_clauses_not_monad(xs,scope){
    const [ssraw,x]=unmake_monad(evalIfStatement_clauses(xs,scope))
    const ss=ssraw.concat([x]).filter(not_null_statement_p)
    if(ss.length==1){
	return ss[0]
    }
    return phpast_make_phpblock(ss)
}
function evalIfStatement_clauses(xs,scope){
    assert_true(xs.length>0)
    const head=xs[0]
    if(head.type==='IfClause'||head.type==='ElseifClause'){
	return monad_bind(
	    evalExpression(head.condition,scope),
	    (result_condition)=>
		monad_return({kind: 'if',
			      test:result_condition,
			      body:eval_emu_block_not_monad(head.body,scope),
			      alternate: xs.length===1?null:evalIfStatement_clauses_not_monad(xs.slice(1),scope),
			      shortForm: false}))
    }else{
	assert_equal(head.type, 'ElseClause')
	assert_equal(xs.length,1)
	return monad_return(eval_emu_block_not_monad(head.body,scope))
    }
}
function evalReturnStatement(x,scope){
    assert_equal(x.type,'ReturnStatement')
    const args=x.arguments
    if(args.length===0){
	return monad_return({ kind: 'return', expr: null })
    }
    assert_equal(args.length,1,()=>`Unsupported multiple return values at ${print_loc_of(x)}`)
    const arg=args[0]
    return monad_bind(evalExpression(arg,scope),(x)=>monad_return({ kind: 'return', expr: x }))
}
function luaast_is_AssignmentStatement_array_push_p(x,scope){
    if(x.type==='IndexExpression'){
	const {base, index}=x
	if(base.type==='Identifier'&&index.type==='BinaryExpression'&&index.operator==='+'){
            const {left,right}=index
            if(left.type==='UnaryExpression'&&right.type==='NumericLiteral'&&left.operator==='#'&&right.value===1){
		const {argument:arg}=left
		return arg.type==='Identifier'&&arg.name===base.name
            }
	}
    }
    return false
}
function phpast_un_parenthesis(x){
    return x.kind==='parenthesis'?phpast_un_parenthesis(x.inner):x
}
function evalAssignmentStatement(x,scope){
    assert_equal(x.type,'AssignmentStatement')
    const variables=x.variables
    const inits=x.init
    if(variables.length>1 && inits.length===1 && inits[0].type==='CallExpression' && inits[0].base.type==='Identifier' && inits[0].base.name==='unpack'){
	const {arguments:args}=inits[0]
	if(args.length===1){
	    const [arg]=args
	    const tmpvar_name=new_tmpvar_do()
	    const tmpvar={ type: 'Identifier', name:tmpvar_name}
	    const stats=[]
	    stats.push({type:'LocalStatement',variables:[tmpvar],init:[arg]})
	    for(let i=0;i<variables.length;i++){
		const luai=i+1
		stats.push({type:'AssignmentStatement',
			    variables:[variables[i]],
			    init:[
				{ type: 'IndexExpression',
				  base:tmpvar,
				  index:
				  { type: 'NumericLiteral',
				    value: luai,
				    raw:luai.toString()}}]})
	    }
	    return make_monad(evalStatement_s(stats,scope),null_statement)
	}
    }
    assert_equal(variables.length,1,()=>`Unsupported multiple assignment at ${print_loc_of(x)}`)
    assert_equal(inits.length,1,()=>`Unsupported multiple assignment at ${print_loc_of(x)}`)
    const variable=variables[0]
    const init=inits[0]
    if(luaast_is_AssignmentStatement_array_push_p(variable,scope)){
	return monad_bind(evalExpression_to_const(variable.base,scope),
			  (result_1)=>monad_bind(
			      evalExpression(init,scope),
                              (right)=>
				  monad_return({kind: 'assign',
						operator: '=',
						left: phpast_un_parenthesis(phpast_luaarray_lookup(result_1,phpast_luaarray_length(result_1))),
						right: right })))
    }
    return monad_bind(evalExpression(variable,scope),
		      (left)=>monad_bind(evalExpression(init,scope),
					 (right)=>
					 monad_return({kind: 'assign',operator: '=',left: phpast_un_parenthesis(left),right: right })))
}
function evalLocalStatement(x,scope){
    assert_equal(x.type,'LocalStatement')
    const variables=x.variables
    const inits=x.init
    if(inits.length===0&&variables.length>0){
	const ss=[]
	for(const variable of variables){
	    assert_equal(variable.type,'Identifier')
	    scope_local_add(scope,variable.name)
	    ss.push({
		kind: 'assign',
		operator: '=',
		left:name_lua2phpast(variable.name,scope),
		right:phpast_NULL })
	}
	return make_monad(ss,null_statement)
    }else{
	for(const variable of variables){
	    assert_equal(variable.type,'Identifier')
	    scope_local_add(scope,variable.name)
	}
	return evalAssignmentStatement({type:'AssignmentStatement',variables:variables,init:inits},scope)
    }
}
const evalExpression_table={
    NumericLiteral: (x,scope)=>monad_return({ kind: 'number', value: x.value.toString() }),
    BooleanLiteral: (x,scope)=>monad_return({ kind: 'boolean', value: x.value }),
    Identifier: (x,scope)=>{
	return monad_return(name_lua2phpast_scope_used_add(x.name,scope))
    },
    FunctionDeclaration:evalFunction,
    BinaryExpression: evalBinaryExpressionLogicalExpression,
    LogicalExpression: evalBinaryExpressionLogicalExpression,
    UnaryExpression: (x,scope)=>{
	const {operator,argument}=x
	if(operator==='#'){return eval_array_string_length(argument,scope)}
	const result_operator=(operator==='not'?'!':operator==='-'?'-':assert_fail(`Unsupported unary operator "${operator}" at ${print_loc_of(x)}`))
	return monad_bind(evalExpression(argument,scope),
			  (result_argument)=>monad_return({kind: 'unary',type: result_operator,what: {kind: 'parenthesis', inner:result_argument}}))
    },
    TableConstructorExpression:evalTableConstructorExpression,
    CallExpression:evalCallExpression,
    TableCallExpression:evalCallExpression,
    IndexExpression:evalIndexExpression,
    StringLiteral:(x,scope)=>{
	const {value}=x
	return monad_return({ kind: 'string', value: value, isDoubleQuote: true })
    },
    NilLiteral:(x,scope)=>monad_return(phpast_NULL),
    MemberExpression:(x,scope)=>{
	assert_equal(x.indexer,'.')
	const {base, identifier} = x
	assert_equal(identifier.type, 'Identifier')
	const {name}=identifier
	return evalExpression({type: 'IndexExpression',base:base,index:{ type: 'StringLiteral', value: name}},scope)
    },
}
function evalExpression(x,scope){
    assert_true(x.type in evalExpression_table,()=>`Unsupported node "${x.type}" at ${print_loc_of(x)}`)
    return evalExpression_table[x.type](x,scope)
}
function evalExpression_not_monad(raw,scope){
    const [ss,x]=unmake_monad(evalExpression(raw,scope))
    assert_equal(ss.filter(not_null_statement_p).length,0)
    return x
}
function evalExpression_not_monad_or_pack(raw,scope){
    const luaexp={ type: 'CallExpression',
	     base:
	     { type: 'FunctionDeclaration',
	       identifier: null,
	       isLocal: false,
	       parameters: [],
	       body: [{ type: 'ReturnStatement',arguments: [ raw ]}],
	       inParens: true },
	     arguments: [], }
    const ret0=evalExpression_not_monad(luaexp,scope)
    assert_equal(ret0.kind, 'call')
    assert_equal(ret0.arguments.length,0)
    assert_equal(ret0.what.kind, 'parenthesis')
    assert_equal(ret0.what.inner.kind, 'closure')
    assert_equal(ret0.what.inner.arguments.length, 0)
    const {body}=ret0.what.inner
    assert_equal(body.kind, 'block')
    const {children}=body
    if(children.length===1){
	const chi=children[0]
	assert_equal(chi.kind, 'return')
	return chi.expr
    }
    return ret0
}
function evalExpression_to_const(x,scope){// 求值爲無副作用的。
    //WIP
    if(x.type==='Identifier'){
	return monad_return(evalExpression_not_monad(x,scope))
    }
    const tmpvar={ type: 'Identifier', name: new_tmpvar_do()}
    return monad_bind(
	evalLocalStatement({ type: 'LocalStatement',variables: [tmpvar], init:[x]}, scope),
	(ss)=>monad_bind(
	    evalExpression(tmpvar,scope),
	    (v)=>make_monad(ss,v)))
}
function eval_array_string_length(x,scope){
    const b3=luaast_string_array_p3(x,scope)
    if(b3==='string'){
	return monad_bind(evalExpression(x,scope),(result_x)=>monad_return({
	    kind: 'call',
	    what: { kind: 'identifier', resolution: 'uqn', name: 'strlen' },
	    arguments: [result_x]}))
    }else if(b3==='array'){
	return monad_bind(evalExpression(x,scope),(result_x)=>monad_return(phpast_luaarray_length(result_x)))
    }else{
	assert_equal(b3,'unknown')
	return monad_bind(evalExpression_to_const(x,scope),(result_x)=>monad_return(
	    phpast_runtime_ifThenElse(phpast_runtime_is_string(result_x),
				      {
					  kind: 'call',
					  what: { kind: 'identifier', resolution: 'uqn', name: 'strlen' },
					  arguments: [result_x]},
				      phpast_luaarray_length(result_x))))
    }
}
function evalIndexExpression(x,scope){
    assert_equal(x.type, 'IndexExpression')
    const {base, index}=x
    const b3=luaast_number_p3(index,scope)
    return monad_bind(
	evalExpression(base, scope),
	(result_base)=>{
	    if(b3===true){
		return monad_bind(
		    evalExpression(luaast_number_sub1(index,scope),scope),
		    (result_index)=>monad_return(phpast_luaarray_lookup(result_base,result_index)))
	    }else if(b3===false){
		return monad_bind(
		    evalExpression(index,scope),
		    (result_index)=>monad_return(phpast_luaarray_lookup(result_base,result_index)))
	    }else{
		assert_equal(b3,'unknown')
		return monad_bind(
		    evalExpression_to_const(index,scope),
		    (result_index)=>monad_return(
			phpast_luaarray_lookup(result_base,
					      phpast_runtime_ifThenElse(phpast_runtime_is_int(result_index),phpast_number_sub1(result_index),result_index))))
	    }
	})
}
const builtin_lib={
    table:{
	remove:(args,scope)=>{
	    if(args.length===1){
		const [x]=args
		return monad_bind(
		    evalExpression(x,scope),
		    (result_x)=>
			monad_return({
			    kind: 'call',
			    what: { kind: 'identifier', resolution: 'uqn', name: 'array_pop' },
			    arguments:[phpast_luaarray2phparray(result_x)]}))
	    }
	},
	insert:(args,scope)=>{
	    if(args.length===3){
		const [list,pos,value]=args
		return monad_bind(
		    evalExpression(list,scope),
		    (result_list)=>
			monad_bind(
			    evalExpression(luaast_number_sub1(pos,scope),scope),
			    (result_pos_sub1)=>
				monad_bind(
				    evalExpression(value,scope),
				    (result_value)=>
					monad_return({
					    kind: 'call',
					    what: { kind: 'identifier', resolution: 'uqn', name: 'array_splice' },
					    arguments:[phpast_luaarray2phparray(result_list),
						       result_pos_sub1,
						       { kind: 'number', value: '0' },
						       {
							   kind: 'array',
							   items: [ { kind: 'entry', key: null, value: result_value } ],
							   shortForm: true }
						      ]}))))
	    }
	}
    },
    string:{
	sub:(args,scope)=>{
	    if(args.length===3){
		const [a1,a2,a3]=args
		return monad_bind(
		    evalExpression(a1,scope),
		    (result_a1)=>
			monad_bind(
			    evalExpression_to_const(luaast_number_sub1(a2,scope),scope),
			    (result_a2_sub1)=>
				monad_bind(
				    evalExpression(a3,scope),
				    (result_a3)=>
					monad_return({
					    kind: 'call',
					    what: { kind: 'identifier', resolution: 'uqn', name: 'substr' },
					    arguments:
					    [ result_a1,
					      result_a2_sub1,
					      phpast_number_sub(result_a3,result_a2_sub1)]}))))
	    }
	}
    }
}
function evalCallExpression(x,scope){
    if(x.type==='TableCallExpression'){
	x.type='CallExpression'
	x.arguments=[x.arguments]
    }
    assert_equal(x.type,'CallExpression')
    const {base,arguments:args}=x
    if(base.type==='Identifier'){
	const {name}=base
	if(name==='error'){
	    const err=()=>`"error()" can only receive one string argument. At ${print_loc_of(x)}`
	    assert_equal(args.length,1,err)
	    const [arg]=args
	    assert_notEqual(arg.type,'VarargLiteral',err)
	    return monad_bind(evalExpression(arg,scope),
			      (result_arg)=>monad_return({
				  kind: 'throw',
				  what: {
				      kind: 'new',
				      what: { kind: 'identifier', resolution: 'uqn', name: 'Exception' },
				      arguments: [ result_arg ] } }))
	}else if(name==='print'){
	    const err=()=>`"print()" can only receive one string argument. At ${print_loc_of(x)}`
	    assert_equal(args.length,1,err)
	    const [arg]=args
	    assert_notEqual(arg.type,'VarargLiteral',err)
	    return monad_bind(evalExpression(arg,scope),
			      (result_arg)=>monad_return({kind: 'echo',
							  arguments:
							  [ result_arg,
							    { kind: 'string', value: '\n', isDoubleQuote: true } ] }))
	}else if(name==='tostring'){
	    const err=()=>`"tostring()" can only receive one string argument. At ${print_loc_of(x)}`
	    assert_equal(args.length,1,err)
	    const [arg]=args
	    assert_notEqual(arg.type,'VarargLiteral',err)
	    return monad_bind(evalExpression(arg,scope),
			      (result_arg)=>monad_return({kind: 'parenthesis',
							  inner:
							  {kind: 'cast',
							   type: 'string',
							   what: result_arg}}))
	}
    }else if(base.type==='MemberExpression'){
	const {indexer, identifier, base:base1}=base
	if(indexer==='.'&&base1.type==='Identifier'&&identifier.type==='Identifier'&&base1.name in builtin_lib){
	    const {name}=identifier
	    const t2=builtin_lib[base1.name]
	    if(name in t2){
		const ret=t2[name](args,scope)
		if(ret!=null){ // `undefined!=null` is false.
		    return ret
		}
	    }
	}
    }
    return monad_bind(
	evalExpression(base,scope),
	(result_base)=>monad_bind(
	    monad_mapM((arg)=>{
		if(arg.type==='VarargLiteral'){
		    assert_equal(arg.value,'...')
		    assert_equal(arg.raw,'...')
		    return monad_return({kind: 'variadic',what:name_lua2phpast(luatmpvar_vararg,no_scope)})
		}else{
		    return evalExpression(arg,scope)
		}
	    },args),
	    (result_args)=>monad_return({kind: 'call',what:result_base,arguments:result_args})))
}
function evalBinaryExpressionLogicalExpression(x,scope){
    assert_true(x.type==='BinaryExpression' || x.type==='LogicalExpression')
    const {operator,left,right}=x
    const operators_table={
	"+":"+",
	"-":"-",
	"*":"*",
	"/":"/",
	"or":"||",
	"and":"&&",
	"==":"===",
	"~=":"!==",
	"<":"<",
	">":">",
	"<=":"<=",
	">=":">=",
	"..":".",
    }
    const result_operator=operator in operators_table?operators_table[operator]:assert_fail(`Unsupported binary operator "${operator}" at ${print_loc_of(x)}`)
    return monad_bind(evalExpression(left,scope),
		      (result_left)=>monad_bind(evalExpression(right,scope),
						(result_right)=>
						monad_return({kind: 'parenthesis', inner:{ kind: 'bin', type: result_operator, left: result_left, right: result_right }})))
}
function evalTableConstructorExpression(x,scope){
    assert_equal(x.type,'TableConstructorExpression')
    const fields=x.fields
    if(fields.length===1&&fields[0].type==='TableValue'&&fields[0].value.type==='VarargLiteral'){
	assert_equal(fields[0].value.value,'...')
	assert_equal(fields[0].value.raw,'...')
	return monad_return(phpast_make_luaarray(name_lua2phpast(luatmpvar_vararg,no_scope)))
    }
    return monad_bind(
	monad_mapM((field)=>{
	    const {type,key=null,value}=field
	    if(type==='TableKey'){
		return monad_bind(evalExpression(key,scope),
				  (result_key)=>monad_bind(evalExpression(value,scope),
							   (result_value)=>monad_return({ kind: 'entry', key:result_key, value: result_value })))
	    }else if(type==='TableKeyString'){
		assert_equal(key.type,'Identifier')
		const {name}=key
		return monad_bind(evalExpression(value,scope),
				  (result_value)=>monad_return({ kind: 'entry',
								 key:{ kind: 'string', value: name, isDoubleQuote: true },
								 value: result_value }))
	    }else{
		assert_equal(type,'TableValue')
		return monad_bind(evalExpression(value,scope),
				  (result_value)=>monad_return({ kind: 'entry', key:null, value: result_value }))
	    }
	},fields),
	(result_fields)=>monad_return(phpast_make_luaarray({kind: 'array',items:result_fields,shortForm: true})))
}
function evalFunction(x,scope){// 無名函數
    assert_equal(x.type,'FunctionDeclaration')
    assert_equal(x.identifier,null)
    const closure_scope=new_phpfunction_scope(scope)
    const result_arguments=[]
    for(const identifier of x.parameters){
	if(identifier.type==='VarargLiteral'){
	    assert_equal(identifier.value, '...')
	    assert_equal(identifier.raw, '...')
	    scope_local_add(closure_scope,luatmpvar_vararg)
	    result_arguments.push({
		kind: 'parameter',
		name: name_lua2php(luatmpvar_vararg,no_scope),
		value: null,
		type: null,
		byref: false,
		variadic: true,
		nullable: false })
	}else{
	    assert_equal(identifier.type,'Identifier')
	    scope_local_add(closure_scope,identifier.name)
	    result_arguments.push({
		kind: 'parameter',
		name: name_lua2php(identifier.name,closure_scope),
		value: phpast_NULL,
		type: null,
		byref: false,
		variadic: false,
		nullable: false })
	}
    }
    const result_body={ // 對used_nonlocals有副作用。
	kind: 'block',
	children: x.body.map((body)=>{
	    const [ss,x]=unmake_monad(evalStatement(body,closure_scope))
	    return ss.concat([x])}).flat().filter(not_null_statement_p)}
    const result_uses=[]
    for(const id of scope_php_used_nonlocals_get(closure_scope)){
	result_uses.push({ kind: 'variable', name: name_lua2php(id,scope), byref: true, curly: false })
	scope_used_add(scope,id)
    }
    return monad_return({kind: 'parenthesis', inner:{kind: 'closure',
						     uses: result_uses,
						     arguments: result_arguments,
						     body:result_body}})
}
