#!/usr/bin/env node

const lua2php=require('../lua2php.js')
const fs=require('fs')
const file=require('yargs')
      .usage('Usage: $0 <file?>')
      .help('h').alias('h', 'help')
      .example('$0 a.lua','Transpile a.lua to php')
      .demandCommand(1, 'Invalid options, specify a file')
      .argv._[0]
const code=fs.readFileSync(file,'utf-8')

console.log(lua2php.post(lua2php.evalChunk(lua2php.pre(code))))
